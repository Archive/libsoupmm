/*
 * libsoupmm - a C++ wrapper for libsoup
 *
 * Copyright (c) 2009  Siavash Safi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBSOUPMM_H
#define LIBSOUPMM_H

#include <glibmm.h>

#include <libsoupmmconfig.h>
#include <libsoupmm/address.h>
#include <libsoupmm/auth.h>
#include <libsoupmm/cookie.h>
#include <libsoupmm/cookie-jar.h>
#include <libsoupmm/cookie-jar-text.h>
#include <libsoupmm/date.h>
#include <libsoupmm/enums.h>
#include <libsoupmm/form.h>
#include <libsoupmm/message.h>
#include <libsoupmm/message-body.h>
#include <libsoupmm/server.h>
#include <libsoupmm/session.h>
#include <libsoupmm/status.h>
#include <libsoupmm/uri.h>
#include <libsoupmm/xmlrpc.h>

#endif /* !LIBSOUPMM_H */
