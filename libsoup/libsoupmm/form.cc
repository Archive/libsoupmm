/* Copyright (c) 2009  Siavash Safi <siavashs@siavashs.org>
 *
 * This file is part of libsoupmm.
 *
 * libsoupmm is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * libsoupmm is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glibmm.h>
#include <libsoupmm/form.h>
#include <libsoup/soup-form.h>

namespace Soup
{

std::string form_encode(const std::map<std::string, std::string>& fields)
{
  std::string encoded;

  for(std::map<std::string, std::string>::const_iterator iter = fields.begin(); iter != fields.end(); ++iter)
  {
    if(!encoded.empty())
      encoded += "&";

    encoded += iter->first + "=" + iter->second;
  }

  return encoded;
}

} // namespace Soup

