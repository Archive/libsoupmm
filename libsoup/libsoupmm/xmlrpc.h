/* Copyright (c) 2009  Siavash Safi <siavashs@siavashs.org>
 *
 * This file is part of libsoupmm.
 *
 * libsoupmm is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * libsoupmm is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBSOUPMM_XMLRPC_H_INCLUDED
#define LIBSOUPMM_XMLRPC_H_INCLUDED

namespace Soup
{

std::string xmlrpc_build_method_call(const std::string& method, const std::list<Glib::ValueBase>& parameters);

bool xmlrpc_parse_method_response(const std::string response, Glib::ValueBase& value, Glib::Error& error);

} // namespace Soup

#endif /* !LIBSOUPMM_XMLRPC_H_INCLUDED */
