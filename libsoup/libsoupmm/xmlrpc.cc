/* Copyright (c) 2009  Siavash Safi <siavashs@siavashs.org>
 *
 * This file is part of libsoupmm.
 *
 * libsoupmm is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * libsoupmm is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glibmm.h>
#include <libsoupmm/xmlrpc.h>
#include <libsoup/soup-xmlrpc.h>

namespace Soup
{

std::string xmlrpc_build_method_call(const std::string& method, const std::list<Glib::ValueBase>& parameters)
{
  GValueArray *params = g_value_array_new(1);

  for(std::list<Glib::ValueBase>::const_iterator iter = parameters.begin(); iter != parameters.end(); ++iter)
  {
    g_value_array_append(params, iter->gobj());
  }
  
  std::string method_call = Glib::convert_const_gchar_ptr_to_stdstring(soup_xmlrpc_build_method_call(method.c_str(), params->values, params->n_values));
  g_value_array_free (params);

  return method_call;
}

bool xmlrpc_parse_method_response(const std::string response, Glib::ValueBase& value, Glib::Error& error)
{
  GError* c_error = error.gobj();
  return soup_xmlrpc_parse_method_response(response.c_str(), response.size(), value.gobj(), &c_error);
}

} // namespace Soup

