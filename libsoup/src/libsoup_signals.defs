;; From SoupAddress

(define-property name
  (of-object "SoupAddress")
  (prop-type "GParamString")
  (docs "Hostname for this address")
  (readable #t)
  (writable #t)
  (construct-only #t)
)

(define-property family
  (of-object "SoupAddress")
  (prop-type "GParamEnum")
  (docs "Address family for this address")
  (readable #t)
  (writable #t)
  (construct-only #t)
)

(define-property port
  (of-object "SoupAddress")
  (prop-type "GParamInt")
  (docs "Port for this address")
  (readable #t)
  (writable #t)
  (construct-only #t)
)

(define-property physical
  (of-object "SoupAddress")
  (prop-type "GParamString")
  (docs "IP address for this address")
  (readable #t)
  (writable #f)
  (construct-only #f)
)

(define-property sockaddr
  (of-object "SoupAddress")
  (prop-type "GParamPointer")
  (docs "struct sockaddr for this address")
  (readable #t)
  (writable #t)
  (construct-only #t)
)

;; From SoupAuth

(define-signal save-password
  (of-object "SoupAuth")
  (return-type "void")
  (when "first")
  (parameters
    '("const-gchar*" "p0")
    '("const-gchar*" "p1")
  )
)

(define-property scheme-name
  (of-object "SoupAuth")
  (prop-type "GParamString")
  (docs "Authentication scheme name")
  (readable #t)
  (writable #f)
  (construct-only #f)
)

(define-property realm
  (of-object "SoupAuth")
  (prop-type "GParamString")
  (docs "Authentication realm")
  (readable #t)
  (writable #t)
  (construct-only #t)
)

(define-property host
  (of-object "SoupAuth")
  (prop-type "GParamString")
  (docs "Authentication host")
  (readable #t)
  (writable #t)
  (construct-only #t)
)

(define-property is-for-proxy
  (of-object "SoupAuth")
  (prop-type "GParamBoolean")
  (docs "Whether or not the auth is for a proxy server")
  (readable #t)
  (writable #t)
  (construct-only #t)
)

(define-property is-authenticated
  (of-object "SoupAuth")
  (prop-type "GParamBoolean")
  (docs "Whether or not the auth is authenticated")
  (readable #t)
  (writable #f)
  (construct-only #f)
)

;; From SoupCookieJar

(define-signal changed
  (of-object "SoupCookieJar")
  (return-type "void")
  (when "first")
  (parameters
    '("SoupCookie*" "p0")
    '("SoupCookie*" "p1")
  )
)

(define-property read-only
  (of-object "SoupCookieJar")
  (prop-type "GParamBoolean")
  (docs "Whether or not the cookie jar is read-only")
  (readable #t)
  (writable #t)
  (construct-only #t)
)

;; From SoupCookieJarText

(define-property read-only
  (of-object "SoupCookieJarText")
  (prop-type "GParamBoolean")
  (docs "Whether or not the cookie jar is read-only")
  (readable #t)
  (writable #t)
  (construct-only #t)
)

(define-property filename
  (of-object "SoupCookieJarText")
  (prop-type "GParamString")
  (docs "Cookie-storage filename")
  (readable #t)
  (writable #t)
  (construct-only #t)
)

;; From SoupMessage

(define-signal wrote-informational
  (of-object "SoupMessage")
  (return-type "void")
  (when "first")
)

(define-signal wrote-headers
  (of-object "SoupMessage")
  (return-type "void")
  (when "first")
)

(define-signal wrote-chunk
  (of-object "SoupMessage")
  (return-type "void")
  (when "first")
)

(define-signal wrote-body-data
  (of-object "SoupMessage")
  (return-type "void")
  (when "first")
  (parameters
    '("SoupBuffer*" "p0")
  )
)

(define-signal wrote-body
  (of-object "SoupMessage")
  (return-type "void")
  (when "first")
)

(define-signal got-informational
  (of-object "SoupMessage")
  (return-type "void")
  (when "first")
)

(define-signal got-headers
  (of-object "SoupMessage")
  (return-type "void")
  (when "first")
)

(define-signal got-chunk
  (of-object "SoupMessage")
  (return-type "void")
  (when "first")
  (parameters
    '("SoupBuffer*" "p0")
  )
)

(define-signal got-body
  (of-object "SoupMessage")
  (return-type "void")
  (when "first")
)

(define-signal content-sniffed
  (of-object "SoupMessage")
  (return-type "void")
  (when "first")
  (parameters
    '("const-gchar*" "p0")
    '("GHashTable*" "p1")
  )
)

(define-signal restarted
  (of-object "SoupMessage")
  (return-type "void")
  (when "first")
)

(define-signal finished
  (of-object "SoupMessage")
  (return-type "void")
  (when "first")
)

(define-property method
  (of-object "SoupMessage")
  (prop-type "GParamString")
  (docs "The message's HTTP method")
  (readable #t)
  (writable #t)
  (construct-only #f)
)

(define-property uri
  (of-object "SoupMessage")
  (prop-type "GParamBoxed")
  (docs "The message's Request-URI")
  (readable #t)
  (writable #t)
  (construct-only #f)
)

(define-property http-version
  (of-object "SoupMessage")
  (prop-type "GParamEnum")
  (docs "The HTTP protocol version to use")
  (readable #t)
  (writable #t)
  (construct-only #f)
)

(define-property flags
  (of-object "SoupMessage")
  (prop-type "GParamFlags")
  (docs "Various message options")
  (readable #t)
  (writable #t)
  (construct-only #f)
)

(define-property server-side
  (of-object "SoupMessage")
  (prop-type "GParamBoolean")
  (docs "Whether or not the message is server-side rather than client-side")
  (readable #t)
  (writable #t)
  (construct-only #t)
)

(define-property status-code
  (of-object "SoupMessage")
  (prop-type "GParamUInt")
  (docs "The HTTP response status code")
  (readable #t)
  (writable #t)
  (construct-only #f)
)

(define-property reason-phrase
  (of-object "SoupMessage")
  (prop-type "GParamString")
  (docs "The HTTP response reason phrase")
  (readable #t)
  (writable #t)
  (construct-only #f)
)

;; From SoupSession

(define-signal request-queued
  (of-object "SoupSession")
  (return-type "void")
  (when "first")
  (parameters
    '("SoupMessage*" "p0")
  )
)

(define-signal request-started
  (of-object "SoupSession")
  (return-type "void")
  (when "first")
  (parameters
    '("SoupMessage*" "p0")
    '("SoupSocket*" "p1")
  )
)

(define-signal request-unqueued
  (of-object "SoupSession")
  (return-type "void")
  (when "first")
  (parameters
    '("SoupMessage*" "p0")
  )
)

(define-signal authenticate
  (of-object "SoupSession")
  (return-type "void")
  (when "first")
  (parameters
    '("SoupMessage*" "p0")
    '("SoupAuth*" "p1")
    '("gboolean" "p2")
  )
)

(define-property proxy-uri
  (of-object "SoupSession")
  (prop-type "GParamBoxed")
  (docs "The HTTP Proxy to use for this session")
  (readable #t)
  (writable #t)
  (construct-only #f)
)

(define-property max-conns
  (of-object "SoupSession")
  (prop-type "GParamInt")
  (docs "The maximum number of connections that the session can open at once")
  (readable #t)
  (writable #t)
  (construct-only #f)
)

(define-property max-conns-per-host
  (of-object "SoupSession")
  (prop-type "GParamInt")
  (docs "The maximum number of connections that the session can open at once to a given host")
  (readable #t)
  (writable #t)
  (construct-only #f)
)

(define-property use-ntlm
  (of-object "SoupSession")
  (prop-type "GParamBoolean")
  (docs "Whether or not to use NTLM authentication")
  (readable #t)
  (writable #t)
  (construct-only #f)
)

(define-property ssl-ca-file
  (of-object "SoupSession")
  (prop-type "GParamString")
  (docs "File containing SSL CA certificates")
  (readable #t)
  (writable #t)
  (construct-only #f)
)

(define-property async-context
  (of-object "SoupSession")
  (prop-type "GParamPointer")
  (docs "The GMainContext to dispatch async I/O in")
  (readable #t)
  (writable #t)
  (construct-only #t)
)

(define-property timeout
  (of-object "SoupSession")
  (prop-type "GParamUInt")
  (docs "Value in seconds to timeout a blocking I/O")
  (readable #t)
  (writable #t)
  (construct-only #f)
)

(define-property user-agent
  (of-object "SoupSession")
  (prop-type "GParamString")
  (docs "User-Agent string")
  (readable #t)
  (writable #t)
  (construct-only #f)
)

(define-property idle-timeout
  (of-object "SoupSession")
  (prop-type "GParamUInt")
  (docs "Connection lifetime when idle")
  (readable #t)
  (writable #t)
  (construct-only #f)
)

(define-property add-feature
  (of-object "SoupSession")
  (prop-type "GParamObject")
  (docs "Add a feature object to the session")
  (readable #t)
  (writable #t)
  (construct-only #f)
)

(define-property add-feature-by-type
  (of-object "SoupSession")
  (prop-type "GParamGType")
  (docs "Add a feature object of the given type to the session")
  (readable #t)
  (writable #t)
  (construct-only #f)
)

(define-property remove-feature-by-type
  (of-object "SoupSession")
  (prop-type "GParamGType")
  (docs "Remove features of the given type from the session")
  (readable #t)
  (writable #t)
  (construct-only #f)
)

;; From SoupServer

(define-signal request-started
  (of-object "SoupServer")
  (return-type "void")
  (when "first")
  (parameters
    '("SoupMessage*" "p0")
    '("SoupClientContext*" "p1")
  )
)

(define-signal request-read
  (of-object "SoupServer")
  (return-type "void")
  (when "first")
  (parameters
    '("SoupMessage*" "p0")
    '("SoupClientContext*" "p1")
  )
)

(define-signal request-finished
  (of-object "SoupServer")
  (return-type "void")
  (when "first")
  (parameters
    '("SoupMessage*" "p0")
    '("SoupClientContext*" "p1")
  )
)

(define-signal request-aborted
  (of-object "SoupServer")
  (return-type "void")
  (when "first")
  (parameters
    '("SoupMessage*" "p0")
    '("SoupClientContext*" "p1")
  )
)

(define-property port
  (of-object "SoupServer")
  (prop-type "GParamUInt")
  (docs "Port to listen on")
  (readable #t)
  (writable #t)
  (construct-only #t)
)

(define-property interface
  (of-object "SoupServer")
  (prop-type "GParamObject")
  (docs "Address of interface to listen on")
  (readable #t)
  (writable #t)
  (construct-only #t)
)

(define-property ssl-cert-file
  (of-object "SoupServer")
  (prop-type "GParamString")
  (docs "File containing server SSL certificate")
  (readable #t)
  (writable #t)
  (construct-only #t)
)

(define-property ssl-key-file
  (of-object "SoupServer")
  (prop-type "GParamString")
  (docs "File containing server SSL key")
  (readable #t)
  (writable #t)
  (construct-only #t)
)

(define-property async-context
  (of-object "SoupServer")
  (prop-type "GParamPointer")
  (docs "The GMainContext to dispatch async I/O in")
  (readable #t)
  (writable #t)
  (construct-only #t)
)

(define-property raw-paths
  (of-object "SoupServer")
  (prop-type "GParamBoolean")
  (docs "If %TRUE, percent-encoding in the Request-URI path will not be automatically decoded.")
  (readable #t)
  (writable #t)
  (construct-only #t)
)

(define-property server-header
  (of-object "SoupServer")
  (prop-type "GParamString")
  (docs "Server header")
  (readable #t)
  (writable #t)
  (construct-only #f)
)

