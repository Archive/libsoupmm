dnl Copyright (c) 2009  Siavash Safi <siavashs@siavashs.org>
dnl This file is part of libsoupmm.

_CONVERSION(`SoupAuth*',`const Glib::RefPtr<Auth>&',`Glib::wrap($3)')
_CONVERSION(`const Glib::RefPtr<Auth>&',`SoupAuth*',__CONVERT_REFPTR_TO_P)

_CONVERSION(`SoupAddress*',`Glib::RefPtr<Address>',`Glib::wrap($3)')

_CONVERSION(`Buffer',`SoupBuffer*',($3).gobj())
_CONVERSION(`Buffer&',`SoupBuffer*',($3).gobj())
_CONVERSION(`SoupBuffer*',`Buffer',($2)($3))
_CONVERSION(`SoupBuffer*',`Buffer&',($2)($3))

_CONVERSION(`SoupClientContext*',`ClientContext',`Glib::wrap($3)')
_CONVERSION(`ClientContext',`SoupClientContext*',($3).gobj())

_CONVERSION(`Cookie&',`SoupCookie*',($3).gobj())
_CONVERSION(`SoupCookie*',`Cookie&',($2)($3))

_CONVERSION(`Date&',`SoupDate*',($3).gobj())

_CONVERSION(`SoupMessage*',`Glib::RefPtr<Message>',`Glib::wrap($3)')
_CONVERSION(`SoupMessage*',`const Glib::RefPtr<Message>&',`Glib::wrap($3)')
_CONVERSION(`const Glib::RefPtr<Message>&',`SoupMessage*',__CONVERT_REFPTR_TO_P)

_CONVERSION(`SoupSession*',`Glib::RefPtr<Session>',`Glib::wrap($3)')
_CONVERSION(`const Glib::RefPtr<SessionFeature>&',`SoupSessionFeature*',__CONVERT_REFPTR_TO_P)
_CONVERSION(`SoupSessionFeature*',`Glib::RefPtr<SessionFeature>',`Glib::wrap($3)')

_CONVERSION(`URI&',`SoupURI*',($3).gobj())
_CONVERSION(`const URI&',`SoupURI*',($3).gobj())
_CONVERSION(`SoupURI*',`URI',($2)($3))

_CONVERSION(`return-char*',`std::string',`Glib::convert_return_gchar_ptr_to_stdstring($3)')
_CONVERSION(`GMainContext*',`Glib::RefPtr<Glib::MainContext>',`Glib::wrap($3)')

_CONV_ENUM(Soup,DateFormat)
_CONV_ENUM(Soup,HTTPVersion)
_CONV_ENUM(Soup,MemoryUse)
_CONV_ENUM(Soup,MessageFlags)
