/* Copyright (c) 2009  Siavash Safi <siavashs@siavashs.org>
 *
 * This file is part of libsoupmm.
 *
 * libsoupmm is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 2.1 of the License,
 * or (at your option) any later version.
 *
 * libsoupmm is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glibmm_generate_extra_defs/generate_extra_defs.h>
#include <glibmm.h>
#include <libsoup/soup.h>
#include <iostream>

int main(int, char**)
{
  Glib::init();
  Glib::thread_init();

  std::cout << get_defs(SOUP_TYPE_ADDRESS);
  std::cout << get_defs(SOUP_TYPE_AUTH);
  std::cout << get_defs(SOUP_TYPE_COOKIE_JAR);
  std::cout << get_defs(SOUP_TYPE_COOKIE_JAR_TEXT);
  std::cout << get_defs(SOUP_TYPE_MESSAGE);
  std::cout << get_defs(SOUP_TYPE_SESSION);
  std::cout << get_defs(SOUP_TYPE_SERVER);

  return 0;
}
