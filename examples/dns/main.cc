#include <iostream>
#include <stdlib.h>

#include <libsoupmm.h>

Glib::RefPtr<Glib::MainLoop> loop(0);
static int nlookups = 0;

void resolve_callback(guint status, const Glib::RefPtr<Soup::Address>& address)
{
  if (status == Soup::STATUS_OK)
  {
    std::cout << "Name:    " << address->get_name() << std::endl;
    std::cout << "Address: " << address->get_physical() << std::endl;
  }
  else
  {
    std::cout << "Name:    " << address->get_name() << std::endl;
    std::cout << "Error:   " << Soup::status_get_phrase(status) << std::endl;
  }
	std::cout << std::endl;

	nlookups--;
	if(nlookups == 0)
		loop->quit();
}

void usage()
{
	std::cerr << "Usage: dns hostname ..." << std::endl;
	exit(1);
}

int main(int argc, char **argv)
{
	if(argc < 2)
		usage();

  Glib::init();
	Glib::thread_init();

	for(int i = 1; i < argc; i++)
  {
		Glib::RefPtr<Soup::Address> address = Soup::Address::create(argv[i], 0);
		if(!address)
    {
			std::cerr << "Could not parse address " << argv[i] << std::endl;
			exit(1);
		}

		address->resolve_async(sigc::bind(sigc::ptr_fun(resolve_callback), address));
		nlookups++;
	}

  // Create and run a main loop
	loop = Glib::MainLoop::create();
  loop->run();

	return 0;
}
